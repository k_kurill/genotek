<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use app\models\NewsImport;

/**
 * Description of SiteController
 *
 * @author Кирилл
 */
class SiteController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Lists all Transaction models.
     * @return mixed
     */
    public function actionIndex()
    {
        $importModel = new NewsImport();
        if (Yii::$app->request->isPost) {
            $importModel->file = UploadedFile::getInstance($importModel, 'file');
            $importModel->import();
        }
        return $this->render('index', ['importModel' => $importModel]);
    }
    
}