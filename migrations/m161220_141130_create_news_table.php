<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m161220_141130_create_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news', [
            'id'            => $this->primaryKey(),
            'category_id'   => $this->integer(),
            'active'        => $this->boolean(),
            'date'          => $this->date(),
            'title'         => $this->string(),
            'image'         => $this->string(),
            'description'   => $this->string(),
            'text'          => $this->text(),
        ]);
        $this->addForeignKey('news_category_FK', 'news', 'category_id', 'category', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news');
    }
}
