<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
class m161220_135847_create_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('category', [
            'id'        => $this->primaryKey(),
            'parent_id' => $this->integer(),
            'active'    => $this->boolean(),
            'name'      => $this->string(),
        ]);
        $this->addForeignKey('category_parent_FK', 'category', 'parent_id', 'category', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('category');
    }
}
