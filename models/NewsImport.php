<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use Yii;
use yii\helpers\Json;
use yii\web\UploadedFile;

/**
 * Description of Import
 *
 * @author kirill
 * @property $importData Массив с данными для импорта
 */
class NewsImport extends \yii\base\Model
{

    /**
     * @var UploadedFile file attribute
     */
    public $file;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false],
        ];
    }

    /**
     * Выполняет импорт новостей из JSON строки
     */
    public function import()
    {
        Category::deleteAll();
        if(!$this->file || !$this->validate()){
            return false;
        }
        
        $filename = $this->file->tempName;
        if(!is_readable($filename)) {
            $this->addError('file', 'File is not readable');
            return false;
        }
        
        $json = file_get_contents($filename);
        $data = Json::decode($json);
        $this->importCategory($data);
        return true;
    }

    /**
     * Выполняет импорт новостей из массива
     * @param type $news Массив с новостями
     */
    private function importNews($news, $categoryId)
    {
        foreach ($news as $item){
            $model = new News();
            $model->attributes = $item;
            $model->category_id = $categoryId;
            $model->save();
        }
    }

    /**
     * Выполняет импорт категорий из массива категорий
     * @param type $categories
     */
    private function importCategory($categories, $parentId = null)
    {
        foreach ($categories as $category) {
            $model = new Category();
            $model->attributes = $category;
            $model->parent_id = $parentId;
            $model->save();            
            if(isset($category['news'])) {
                $this->importNews($category['news'], $category['id']);
            }
            if(isset($category['subcategories'])) {
                $this->importCategory($category['subcategories'], $category['id']);
            }
        }
    }

}